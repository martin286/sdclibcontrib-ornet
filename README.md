# README #

This is a contribution fork of SDCLib, a pure java IEEE 11073 SDC Family compatible API.

As former main author of the official project, I will maintain this fork to implement the newest features and bugfixes for research and testing purposes.
Via pull requests I will contribute my work to the official project. For details please refer to https://bitbucket.org/surgitaix/sdclib/src/master/

### What is this repository for? ###

* The master branch contains a stable BETA version

### How do I get set up (Java)? ###

* Create a Java maven project
* Include the following maven respository in your pom.xml
```
<project>
  ...
  <repositories>
    <repository>
      <id>besting-it</id>
      <name>besting-it repo</name>
      <url>http://www.besting-it.de/maven</url>
    </repository>
    ...
  </repositories> 
</project>
```
* Add the following dependency to pom.xml
```
<project>
  ...
  <dependencies>
      <dependency>
          <groupId>org.ornet</groupId>
          <artifactId>SDCLib</artifactId>
          <version>6.1.0c</version>
      </dependency>
      ...
  </dependencies>
</project>
```


### Getting started ###

* For examples, see the test sources: test/org/ornet/sdclib/test
* DemoProviderFactory.java contains a provider using metrics, alerts, contexts and streaming

### Changelog ###

## 6.1.0c ##

* Fixed provider shutdown error
* Added metadata version increase

## 6.0.2c ##

* Introduced async API
* Device type change
* New provider init methods

## 5.4.0c ##

* Permanent subscription to operation invoked reports
* Eventing on demand (depends on present event handlers in consumer)
* Fixed non-unique handle in operation state

## 5.3.2c ##

* Renamed context changed report

## 5.3.1c ##

* Moved OperationInvoked to SetService
* Fixed UDP msg ID filter bug
* Added IP address output to UDP log messages

## 5.2.0c ##

* SDCProvider now implements java.lang.AutoCloseable (try-with-resources)
* Removed SDCConsumer close() - managed by framework
* ServiceManager discovery methods refactoring (discoverEPRAsync, getAvailableSDCConsumerByEPR, getAvailableSDCConsumers)
* Experimental support for space delimited DPWS eventing filters