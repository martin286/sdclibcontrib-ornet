/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.provider;

import static com.bestingit.async.Task.async;
import org.ornet.sdclib.SDCToolbox;
import com.rits.cloning.Cloner;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertDescriptor;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextDescriptor;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertConditionState;
import org.ornet.cdm.AbstractDescriptor;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractOperationDescriptor;
import org.ornet.cdm.GetMdibResponse;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MdState;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.GetMdDescriptionResponse;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.SystemContextDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.cdm.SafetyReqType;
import org.ornet.sdclib.ISDCTransportLayerConfiguration;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.binding.BICEPSTypeConverter;
import org.ornet.sdclib.binding.device.IDeviceBinding;

public final class SDCProvider implements SDCEndpoint, AutoCloseable {

    private final AtomicLong mdibVersion = new AtomicLong();
    
    private MdDescription mddescription = new MdDescription();
    
    private final List<AbstractState> mdibStates = Collections.synchronizedList(new ArrayList<>());
    private final List<AbstractOperationState> operationStates = Collections.synchronizedList(new ArrayList<>());
    
    private final Map<String, SDCProviderHandler> providerHandlers = Collections.synchronizedMap(new HashMap<>());
    
    private IDeviceBinding transportBinding;
    private ISDCTransportLayerConfiguration transportLayerConfiguration;
    
    public SDCProvider() {
        // The transport binding is automatically set by using the default transport configuration, but can be customized
        setTransportLayerConfiguration(SDCLib.getInstance().getDefaultTransportLayerConfig(ISDCTransportLayerConfiguration.class));
    }

    public final void setTransportLayerConfiguration(ISDCTransportLayerConfiguration transportLayerConfiguration) {
        this.transportLayerConfiguration = transportLayerConfiguration;
        transportBinding = transportLayerConfiguration.getDeviceBinding(this);
    }

    public final ISDCTransportLayerConfiguration getTransportLayerConfiguration() {
        return transportLayerConfiguration;
    }
  
    /**
     * Get deep clone of MD description.
     * 
     * @return The description.
     */
    @Override
    public MdDescription getMDDescription() {
        Cloner cloner = new Cloner();
        return cloner.deepClone(mddescription);
    }

    public long getMdibVersion() {
        return mdibVersion.get();
    }  
	
    public void setMDDescription(MdDescription mddescription){
    	this.mddescription = mddescription;
    }
    
    public void addMDS(MdsDescriptor mds) {
        boolean duplicate = false;
        for(MdsDescriptor descriptor : mddescription.getMds()) {
            if(descriptor.getHandle().equals(mds.getHandle())) {
                duplicate = true;
                break;
            }
        }
        if(!duplicate) {
            this.mddescription.getMds().add(mds);
            mdibVersion.incrementAndGet();
        }
    }
    
    public void removeMDS(String handle) {
        if (handle != null) {
            int i = 0;
            boolean match = false;
            for (MdsDescriptor descriptor : mddescription.getMds()) {
                if (descriptor.getHandle().equals(handle)) {
                    match = true;
                    break;
                }
                i++;
            }
            // Remove
            if (match) {
                mddescription.getMds().remove(i);
                mdibVersion.incrementAndGet();
            }
        }
    }
    
    /**
     * Get deep clone of all MD states.
     * 
     * @return The states.
     */
    @Override
    public MdState getMDState() {
        MdState states = new MdState();
        Cloner cloner = new Cloner();
        synchronized(mdibStates) {
            states.getState().addAll(cloner.deepClone(mdibStates));
        }
        synchronized(operationStates) {
            states.getState().addAll(cloner.deepClone(operationStates));
        }
        return states;
    }
    
    public List<AbstractContextState> getContextStates() {
        return getContextStates(null);
    }
    
    public List<AbstractContextState> getContextStates(List<String> handles) {
        return getTypedStates(handles, AbstractContextState.class);
    } 
    
    public List<AbstractAlertState> getAlertStates() {
        return getTypedStates(null, AbstractAlertState.class);
    }     
    
    public List<AbstractAlertState> getAlertStates(List<String> handles) {
        return getTypedStates(handles, AbstractAlertState.class);
    } 

    private <T> List<T> getTypedStates(List<String> handles, Class<T> type) {
        MdState states = handles == null || handles.isEmpty()? getMDState() : getMDState(handles);
        List<T> targetStates = new LinkedList<>();
        Iterator<AbstractState> it = states.getState().iterator();
        while (it.hasNext()) {
            AbstractState next = it.next();
            if (type.isAssignableFrom(next.getClass())) {
                targetStates.add(type.cast(next));                
            }
        }
        return targetStates;
    }       
    
    /**
     * Get deep clone of MD states.
     * 
     * @param handles List of handles to match.
     * @return The states.
     */
    @Override
    public MdState getMDState(List<String> handles) {
        MdState states = new MdState();
        Set<String> handleSet = new HashSet<>(handles);
        synchronized(mdibStates) {
            addToStateContainer(handleSet, states, mdibStates.iterator());
        }   
        synchronized(operationStates) {
            addToStateContainer(handleSet, states, operationStates.iterator());
        }          
        return states;
    }

    private <T extends AbstractState> void addToStateContainer(Set<String> handleSet, MdState states, Iterator<T> it) {
        Cloner cloner = new Cloner();
        while (it.hasNext()) {
            T next = it.next();
            if (handleSet.contains(next.getDescriptorHandle())) {
                AbstractState cloned = cloner.deepClone(next);
                states.getState().add(cloned);
            }
        }
    }
    
    public List<AbstractOperationDescriptor> createSetOperationForDescriptor(AbstractDescriptor descr, MdsDescriptor mds) {
        return createSetOperationForDescriptor(descr, mds, null);
    }
    
    public List<AbstractOperationDescriptor> createSetOperationForDescriptor(AbstractDescriptor descr, MdsDescriptor mds, SafetyReqType safetyReq) {
        return SDCToolbox.createOperationDescriptor(operationStates, descr, mds, safetyReq);
    }
    
    /**
     * Create an MDIB container object. Modifications of this structure will not be reflected into the internal MDIB representation.
     * 
     * @return The MDIB.
     */
    @Override
    public final Mdib getMDIB() {
        Mdib mdib = new Mdib();
        mdib.setSequenceId("0");
        mdib.setMdibVersion(BigInteger.valueOf(getMdibVersion()));
        mdib.setMdState(getMDState());
        mdib.setMdDescription(getMDDescription());
        return mdib;
    }
	
	public void startup(GetMdibResponse mdibResponse, boolean autoCreateSco) {
		initialize(mdibResponse.getMdib(), autoCreateSco);
		startup();
	}		
	
	public void startup(GetMdDescriptionResponse mddResponse, boolean autoCreateSco) {
        Mdib mdib = new Mdib();
        mdib.setSequenceId("0");
        mdib.setMdibVersion(BigInteger.valueOf(0));
        mdib.setMdDescription(mddResponse.getMdDescription());		
		initialize(mdib, autoCreateSco);
		startup();
	}	
	
	public void startup(Mdib mdib, boolean autoCreateSco) {
		initialize(mdib, autoCreateSco);
		startup();
	}
	
    private void initialize(Mdib mdib, boolean autoCreateSco) {
		Cloner cloner = new Cloner();
		mddescription = cloner.deepClone(mdib.getMdDescription());
		mdibStates.clear();
		mdib.getMdState().getState().forEach(state -> { 
			if (state instanceof AbstractOperationState)
				operationStates.add(cloner.deepClone((AbstractOperationState)state));
			else
				mdibStates.add(cloner.deepClone(state));
		});
		if (autoCreateSco) {
			operationStates.clear();
			final Map<MdsDescriptor, List<AbstractMetricDescriptor>> metrics = SDCToolbox.collectAllMetricDescriptors(this);
			metrics.keySet().forEach(mds -> {
				metrics.get(mds).forEach(descr -> {
					if (SDCToolbox.isMetricChangeAllowed(this, SDCToolbox.findMetricState(this, descr.getHandle())))
						createSetOperationForDescriptor(descr, mds);
				});
			});
			Map<MdsDescriptor, Set<AlertConditionDescriptor>> alertConds = SDCToolbox.collectAllAlertConditionDescriptorsMds(this);
			alertConds.keySet().forEach(mds -> {
				alertConds.get(mds).forEach(descr -> {
					createSetOperationForDescriptor(descr, mds);
				});
			});
			Map<MdsDescriptor, Set<AlertConditionDescriptor>> alertSignl = SDCToolbox.collectAllAlertConditionDescriptorsMds(this);
			alertSignl.keySet().forEach(mds -> {
				alertSignl.get(mds).forEach(descr -> {
					createSetOperationForDescriptor(descr, mds);
				});
			});
		}
	}   	
   
    public void startup() {
        SDCLib.getInstance().getLogger().log(Level.INFO, "Provider startup...");
        try {
            if (!transportBinding.init()) {
                Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, "Fatal error - provider init failed!");
                return;
            }
			if (mdibStates.size() == 0) {
				synchronized(providerHandlers) {
					for (SDCProviderHandler next : providerHandlers.values()) {
						if (next instanceof SDCProviderMDStateHandler) {
							final AbstractState newState = setDefaultStateValues(((SDCProviderMDStateHandler)next).getInitalClonedState());
							mdibStates.add(newState);
							transportBinding.initState(newState);
						}
					}
				}				
			}
            // Check for valid provider MDIB
            if (SDCLib.getInstance().isSchemaValidationEnabled()) {
                GetMdibResponse response = new GetMdibResponse();
                response.setSequenceId("0");
                final Mdib mdib = getMDIB();
                mdib.setSequenceId("0");
                response.setMdib(mdib);
                response.setSequenceId(mdib.getSequenceId());
                BICEPSTypeConverter.toString(response);
            }
            transportBinding.start();
        } catch (Exception ex) {
            Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isRunning() {
        return transportBinding.isRunning();
    }
    
    @Override
    public void close() throws Exception {
        SDCLib.getInstance().getLogger().log(Level.INFO, "Provider shutdown...");
        try {
            transportBinding.stop();
			mdibStates.clear();
        } catch (Exception ex) {
            Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private <T extends AbstractState> boolean updateInternalMatchingState(AbstractState newState, List<T> list) {
        AbstractState temp = null;
        synchronized(list) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
               AbstractState next = it.next();
               if (next.getDescriptorHandle().equals(newState.getDescriptorHandle())) {
                   temp = next;
                   break;
               }
            }
        }
        if (temp == null)
            return false;
        list.remove((T)temp);
        newState.setStateVersion(temp.getStateVersion().add(BigInteger.ONE));
        Cloner cloner = new Cloner();
        list.add((T)cloner.deepClone(setDefaultStateValues(newState)));
        mdibVersion.incrementAndGet();        
        return true;
    }    
    
    private boolean updateInternalMatchingState(AbstractState newState) {
        if (!updateInternalMatchingState(newState, mdibStates))
            return updateInternalMatchingState(newState, operationStates);
        return true;
    }
    
    public void updateState(AbstractState state) {
        updateState(state, true);
    }
    
    public void updateState(AbstractState state, boolean notifyEvents) {
        if (!updateInternalMatchingState(state))
            throw new IllegalStateException("No state to update with given descriptor handle (state handle): " + state.getDescriptorHandle());
        // Call transport state changed
        transportBinding.onStateChanged(state);
        if (!notifyEvents) {
            return;
        }
        // Eventing
        if (state instanceof RealTimeSampleArrayMetricState) {
            transportBinding.getEventSourceBinding().handleStream((RealTimeSampleArrayMetricState)state);
        }        
        else if (state instanceof AbstractMetricState) {
            transportBinding.getEventSourceBinding().handleEpisodicMetricEvent((AbstractMetricState) state, BigInteger.valueOf(mdibVersion.get()));
            evaluateAlertConditions(((AbstractMetricState) state));
        }
        else if (state instanceof AbstractContextState)
            transportBinding.getEventSourceBinding().handleEpisodicContextChangedEvent((AbstractContextState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractAlertState)
            transportBinding.getEventSourceBinding().handleEpisodicAlertEvent((AbstractAlertState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractOperationState)
            throw new IllegalStateException("Eventing not yet supported for operation states, handle: " + state.getDescriptorHandle());
    }  
    
    public synchronized void notifyOperationInvoked(OperationInvocationContext oic, InvocationState is, String operationErrorMsg) {
        transportBinding.getEventSourceBinding().handleOperationInvokedEvent(oic, is, BigInteger.valueOf(mdibVersion.get()), operationErrorMsg);
    }
    
    public void setEndpointReference(String epr) {
        transportBinding.setEndpointReference(epr);
    }
    
    public void addHandler(SDCProviderHandler handler) {
        if (transportBinding.isRunning()) 
            throw new IllegalStateException("Provider is running!");
        handler.setProvider(this);
        providerHandlers.put(handler.getDescriptorHandle(), handler);
    }

    public void removeHandler(SDCProviderHandler handler) {
        if (transportBinding.isRunning()) 
            throw new IllegalStateException("Provider is running!");
        providerHandlers.remove(handler.getDescriptorHandle());
    }
    
    public SDCProviderHandler getHandler(String descriptorHandle) {
        return providerHandlers.get(descriptorHandle);
    }

    public Map<String, SDCProviderHandler> getStateHandlers() {
        return providerHandlers;
    }

    private AbstractState setDefaultStateValues(AbstractState state) {
        if (state.getStateVersion() == null)
            state.setStateVersion(BigInteger.ZERO);
        if (state instanceof AbstractContextState) {
            AbstractContextState acs = (AbstractContextState)state;
            if (acs.getBindingMdibVersion() == null) {
                acs.setBindingMdibVersion(BigInteger.ZERO);
            }
        }
        return state;
    }

    public IDeviceBinding getTransportBinding() {
        return transportBinding;
    }
    
    private void evaluateAlertConditions(AbstractMetricState state) {
        async(() -> {
            Set<AlertConditionDescriptor> acds = SDCToolbox.collectAllAlertConditionDescriptors(this);
            for (AlertConditionDescriptor next : acds) {
                if (next.getSource().contains(state.getDescriptorHandle())) {
                    SDCProviderHandler handler = getHandler(next.getHandle());
                    if (handler == null) {
                        Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, "Error in evaluating alert conditions. Handler missing for: {0}", state.getDescriptorHandle());
                        return;
                    }
                    if (handler instanceof SDCProviderAlertConditionStateHandler) {
                        List<AbstractAlertState> handlerStates = getAlertStates(new ArrayList<>(Arrays.asList(new String [] {next.getHandle()})));
                        if (handlerStates.size() > 0) {
                            ((SDCProviderAlertConditionStateHandler)handler).sourceHasChanged(state, (AlertConditionState)handlerStates.get(0));
                        }
                    }
                }
            }            
        });
    }

    @Override
    public String getEndpointReference() {
        return transportBinding.getEnpointReference();
    }

    public void createFluentAutoSettableHandlers(Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        // Metric states
        for (MdsDescriptor nextMDS : mddescription.getMds()) {
            for (VmdDescriptor nextVmd : nextMDS.getVmd())
                for (ChannelDescriptor nextChn : nextVmd.getChannel())
                    for (AbstractMetricDescriptor nextMet : nextChn.getMetric()) {
                        final AbstractState state = getStateByHandle(states, nextMet.getHandle());
                        if (state == null) {
                            SDCLib.getInstance().getLogger().log(Level.WARNING, "State missing for metric handle: {0}. Skipping state (inital value & SCO operation).", nextMet.getHandle());
                            continue;
                        }                      
                        if (SDCToolbox.isMetricChangeAllowed(this, (AbstractMetricState) state)) {
                            createSetOperationForDescriptor(nextMet, nextMDS);
                        }
                        addHandler(new SDCFluentAutoSettableProviderHandler(nextMet.getHandle(), state, deque));
                    } 
        }
        // Alert states
        for (MdsDescriptor mds : mddescription.getMds()) {
            handleAlertOperations(mds.getAlertSystem(), mds, states, deque);
            for (VmdDescriptor vmd : mds.getVmd()) {
                handleAlertOperations(vmd.getAlertSystem(), mds, states, deque);           
            }                
        }
        // Context states
        for (MdsDescriptor mds : mddescription.getMds()) {
            final SystemContextDescriptor systemContext = mds.getSystemContext();
            if (systemContext != null)
                handleContextOperations(systemContext.getLocationContext(), states, mds, deque);
        }
    }

    private AbstractState getStateByHandle(Collection<AbstractState> states, String handle) {
        Iterator<AbstractState> it = states.iterator();
        while (it.hasNext()) {
            final AbstractState next = it.next();
            if (next.getDescriptorHandle().equals(handle))
                return next;
        }
        return null;
    }

    private void handleAlertOperations(AlertSystemDescriptor alertSystem, MdsDescriptor mds, Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        if (alertSystem == null)
            return;
        handleAlertOperationDescriptors(new ArrayList<>(alertSystem.getAlertCondition()), mds, states, deque);
        handleAlertOperationDescriptors(new ArrayList<>(alertSystem.getAlertSignal()), mds, states, deque);
    }

    private void handleAlertOperationDescriptors(List<AbstractAlertDescriptor> descriptors, MdsDescriptor mds, Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        for (AbstractAlertDescriptor next : descriptors) {
            final AbstractState state = getStateByHandle(states, next.getHandle());
            if (state == null) {
                SDCLib.getInstance().getLogger().log(Level.WARNING, "State missing for alert handle: {0}. Skipping SCO operation creation.", next.getHandle());
                continue;
            }
            createSetOperationForDescriptor(next, mds);
            addHandler(new SDCFluentAutoSettableProviderHandler(next.getHandle(), state, deque));      
        }
    }

    private void handleContextOperations(AbstractContextDescriptor cd, Collection<AbstractState> states, MdsDescriptor mds, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        final AbstractState state = getStateByHandle(states, cd.getHandle());
        if (state == null) {
            SDCLib.getInstance().getLogger().log(Level.WARNING, "State missing for context handle: {0}. Skipping SCO operation creation.", cd.getHandle());
            return;
        }
        createSetOperationForDescriptor(cd, mds);
        addHandler(new SDCFluentAutoSettableProviderHandler(cd.getHandle(), state, deque));           
    }

}
