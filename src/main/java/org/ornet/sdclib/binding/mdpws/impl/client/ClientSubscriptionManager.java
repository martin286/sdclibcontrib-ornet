/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.mdpws.impl.client;

import io.vertx.core.Vertx;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeFactory;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.binding.client.IConsumerEventSinkBinding;
import org.ornet.sdclib.binding.mdpws.impl.ws.WSConstants;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.AbstractSoapMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.RenewMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.SubscribeMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.SubscribeResponseMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.UnsubscribeMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.ClientRoute;

public class ClientSubscriptionManager {
    
    private final MDPWSClient client;
    private final AtomicBoolean connected;
    private final IConsumerEventSinkBinding eventSinkBinding;
    private final String notifyAddress;
    private final Map<String, String> filterIds = new ConcurrentHashMap<>();
    private final Vertx vertx;
    private long subscriptionTimerId;
	private long oirTimerId;
    private boolean requestFailure = false;
	private boolean oirInitialized = false;
    
    private static final int SUBSCRIBE_TIME = 30000;
    private static final int RENEW_TIME = 15000;
    
    public ClientSubscriptionManager(IConsumerEventSinkBinding eventSinkBinding, MDPWSClient client, Vertx vertx, AtomicBoolean connected) {
        this.client = client;
        this.vertx = vertx;
        this.connected = connected;
        this.eventSinkBinding = eventSinkBinding;
        final String http = client.getOptions().isSsl()? "https://" : "http://";
        this.notifyAddress = http + client.getBaseAddress() + ":" + client.getBasePort() + "/" + ClientRoute.REALM;
    }

    public IConsumerEventSinkBinding getEventSinkBinding() {
        return eventSinkBinding;
    }
          
    public void init() {
		initOperationInvoked();
        subscribeAll();
        subscriptionTimerId = vertx.setPeriodic(RENEW_TIME, id -> {
            if (!requestFailure)
                renewAll();
            else {
                subscribeAll();
            }
        });   
    }
	
	public void initOperationInvoked() {
		if (oirInitialized)
			return;
		filterIds.remove(WSConstants.FILTER_OIR);
		subscribe(WSConstants.FILTER_OIR, WSConstants.PORT_TYPE_SET);
        oirTimerId = vertx.setPeriodic(RENEW_TIME, id -> {
            if (!requestFailure)
                renew(WSConstants.FILTER_OIR, WSConstants.PORT_TYPE_SET);
            else {
				filterIds.remove(WSConstants.FILTER_OIR);
                subscribe(WSConstants.FILTER_OIR, WSConstants.PORT_TYPE_SET);
            }
        }); 
		oirInitialized = true;
	}	
    
    private void renewAll() {
        renew(WSConstants.FILTER_EAR, WSConstants.PORT_TYPE_EVT);
        renew(WSConstants.FILTER_ECR, WSConstants.PORT_TYPE_CTX);
        renew(WSConstants.FILTER_EMR, WSConstants.PORT_TYPE_EVT);
    }    

    private void subscribeAll() {
        filterIds.clear();
        subscribe(WSConstants.FILTER_EAR, WSConstants.PORT_TYPE_EVT);
        subscribe(WSConstants.FILTER_ECR, WSConstants.PORT_TYPE_CTX);
        subscribe(WSConstants.FILTER_EMR, WSConstants.PORT_TYPE_EVT);
    }
    
    private void unsubscribeAll() {
        unsubscribe(WSConstants.FILTER_EAR, WSConstants.PORT_TYPE_EVT);
        unsubscribe(WSConstants.FILTER_ECR, WSConstants.PORT_TYPE_CTX);
        unsubscribe(WSConstants.FILTER_EMR, WSConstants.PORT_TYPE_EVT);
		unsubscribe(WSConstants.FILTER_OIR, WSConstants.PORT_TYPE_SET);
    }    
    
    public void subscribe(String filter, String servicePortType) {
        if (!connected.get()) {
            requestFailure = true;
            return;            
        }
        try {
            String identifier = UUID.randomUUID().toString();
            String expires = DatatypeFactory.newInstance().newDuration(SUBSCRIBE_TIME).toString();
            SDCLib.getInstance().getLogger().log(Level.FINER, "Outgoing subscribe request, Filter action: {0}", filter);
            client.postService(servicePortType, new SubscribeMessage(notifyAddress, filter, identifier, expires), (res) -> {
                if (res.statusCode() != 200) {
                    SDCLib.getInstance().getLogger().log(Level.WARNING, "Subscribe request failed: {0}", res.statusMessage());
                    requestFailure = true;
                    return;
                }
                res.bodyHandler((buf) -> {
                    AbstractSoapMessage msg = new AbstractSoapMessage(buf);
                    if (msg.getAction().equals(WSConstants.SOAP_FAULT_ACTION)) {
                        SDCLib.getInstance().getLogger().log(Level.WARNING, "Subscribe request failed: soap fault.");
                        requestFailure = true;
                        return;
                    }
                    SubscribeResponseMessage response = new SubscribeResponseMessage(msg.getContent());
                    filterIds.put(filter, response.getIdentifier());
                    requestFailure = false;
                });
            }, (exc) -> {
                SDCLib.getInstance().getLogger().log(Level.WARNING, "Subscribe request failed: {0} ({1})", new Object[]{filter, exc.toString()});
                requestFailure = true;
            });
        } catch (Exception ex) {
            Logger.getLogger(ClientSubscriptionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void renew(String filter, String servicePortType) {
        if (!connected.get()) {
            requestFailure = true;
            return;            
        }        
        try {
            String expires = DatatypeFactory.newInstance().newDuration(SUBSCRIBE_TIME).toString();
            String identifier = filterIds.get(filter);
            SDCLib.getInstance().getLogger().log(Level.FINER, "Outgoing renew request, identifier: {0}", identifier);
            client.postService(servicePortType, new RenewMessage(identifier, expires), (res) -> {
                if (res.statusCode() != 200) {
                    SDCLib.getInstance().getLogger().log(Level.WARNING, "Renew request failed: {0}", res.statusMessage());
                    requestFailure = true;
                    return;
                }
                res.bodyHandler((buf) -> {
                    AbstractSoapMessage msg = new AbstractSoapMessage(buf);
                    if (msg.getAction().equals(WSConstants.SOAP_FAULT_ACTION)) {
                        SDCLib.getInstance().getLogger().log(Level.WARNING, "Renew request failed: soap fault.");
                        requestFailure = true;
                    }
                });                
            }, (exc) -> {
                SDCLib.getInstance().getLogger().log(Level.WARNING, "Renew request failed: {0} ({1})", new Object[]{filter, exc.toString()});
                requestFailure = true;
            });
        } catch (Exception ex) {
            Logger.getLogger(ClientSubscriptionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    public void unsubscribe(String filter, String servicePortType) {
        if (!connected.get()) {
            requestFailure = true;
            return;            
        }        
        String identifier = filterIds.get(filter);
        SDCLib.getInstance().getLogger().log(Level.FINER, "Outgoing unsubscribe request, identifier: {0}", identifier);
        client.postService(servicePortType, new UnsubscribeMessage(identifier), (res) -> {
            if (res.statusCode() != 200) {
                SDCLib.getInstance().getLogger().log(Level.FINER, "Unsubscribe request failed: {0}", res.statusMessage());
            } else {
                filterIds.remove(filter);
            }
        }, (exc) -> {
            SDCLib.getInstance().getLogger().log(Level.FINER, "Unsubscribe request failed: {0} ({1})", new Object[]{filter, exc.toString()});
            requestFailure = true;
        });
    }        
    
    public void close() {
		if (oirTimerId != -1) {
	        vertx.cancelTimer(oirTimerId);
			oirTimerId = -1;
			oirInitialized = false;
		}
		if (subscriptionTimerId != -1) {
			vertx.cancelTimer(subscriptionTimerId);
			unsubscribeAll();			
		}
    }

	public void deInit() {
		// We leave OIR eventing running until closed, but unsubscribe everything else
		if (subscriptionTimerId != -1) {
			vertx.cancelTimer(subscriptionTimerId);
			subscriptionTimerId = -1;
			unsubscribe(WSConstants.FILTER_EAR, WSConstants.PORT_TYPE_EVT);
			unsubscribe(WSConstants.FILTER_ECR, WSConstants.PORT_TYPE_CTX);
			unsubscribe(WSConstants.FILTER_EMR, WSConstants.PORT_TYPE_EVT);			
		}
	}
    
}
