/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.mdpws.impl.ws;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.binding.mdpws.impl.device.DeviceMsgManager;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.ContextServiceRoute;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.DeviceRoute;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.StateEventRoute;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.GetServiceRoute;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.SetServiceRoute;
import org.ornet.sdclib.binding.mdpws.impl.ws.routes.WaveformServicetRoute;

public class SDCDeviceVerticle extends AbstractVerticle {
    
    private final DeviceMsgManager msgManager;
    private HttpServer server;
    private final Map<String[], List<String>> serviceHosted = new LinkedHashMap<>();
    private final List<String> serviceAddrs = new ArrayList<>();
    private final HttpServerOptions options;
    
    private DeviceRoute deviceRoute = new DeviceRoute();
    private GetServiceRoute getServiceRoute = new GetServiceRoute();
    private SetServiceRoute setServiceRoute = new SetServiceRoute();
    private WaveformServicetRoute waveformEventReportRoute = new WaveformServicetRoute();
    private StateEventRoute eventReportRoute = new StateEventRoute();
    private ContextServiceRoute contextServiceRoute = new ContextServiceRoute();
    private String host;
    private int port;
    private Router router;
    
    public SDCDeviceVerticle(DeviceMsgManager msgManager, HttpServerOptions options) {
        this.msgManager = msgManager;
        this.options = options;
    }
    
    @Override
    public void start(Future<Void> fut)
	{
        router = Router.router(this.vertx);
        host = config().getString("http.host");
        port = config().getInteger("http.port");
		deviceRoute = new DeviceRoute();
		getServiceRoute = new GetServiceRoute();
		setServiceRoute = new SetServiceRoute();
		waveformEventReportRoute = new WaveformServicetRoute();
		eventReportRoute = new StateEventRoute();
		contextServiceRoute = new ContextServiceRoute();		
        
        final String http = options.isSsl()? "https://" : "http://";
        new NIFManager(host, true, true).applyVisitor((NetworkInterface ni, InetAddress addr, String addrStr) -> {
            final String hostedAdr = http + addrStr + ":" + port;
            SDCLib.getInstance().getLogger().log(Level.FINE, "Configuring hosted service address: {0}", hostedAdr);
            serviceAddrs.add(hostedAdr);
        });
        
        serviceHosted.put(new String[] {GetServiceRoute.REALM, GetServiceRoute.TYPE}, serviceAddrs);
        serviceHosted.put(new String[] {SetServiceRoute.REALM, SetServiceRoute.TYPE}, serviceAddrs);
        serviceHosted.put(new String[] {WaveformServicetRoute.REALM, WaveformServicetRoute.TYPE}, serviceAddrs);
        serviceHosted.put(new String[] {StateEventRoute.REALM, StateEventRoute.TYPE}, serviceAddrs);
        serviceHosted.put(new String[] {ContextServiceRoute.REALM, ContextServiceRoute.TYPE}, serviceAddrs);
        
        deviceRoute.configure(router, msgManager, serviceHosted);
        getServiceRoute.configure(router, msgManager, serviceAddrs, WSConstants.WSDL_RESOURCE_BASE_PATH, GetServiceRoute.WSDL);
        setServiceRoute.configure(router, msgManager, serviceAddrs, WSConstants.WSDL_RESOURCE_BASE_PATH, SetServiceRoute.WSDL);
        waveformEventReportRoute.configure(router, msgManager, serviceAddrs, WSConstants.WSDL_RESOURCE_BASE_PATH, WaveformServicetRoute.WSDL);
        eventReportRoute.configure(router, msgManager, serviceAddrs, WSConstants.WSDL_RESOURCE_BASE_PATH, StateEventRoute.WSDL);
        contextServiceRoute.configure(router, msgManager, serviceAddrs, WSConstants.WSDL_RESOURCE_BASE_PATH, ContextServiceRoute.WSDL);
         
        options.setHost(host);
        options.setPort(port);
        
        server = this.vertx.createHttpServer(options);
        server.requestHandler(router::accept).listen((AsyncResult<HttpServer> e) -> {
            SDCLib.getInstance().getLogger().log(Level.INFO, "Device HTTP server {0} running: {1}", new Object[]{http + ":" + host + ":" + port, e.succeeded()});
            fut.complete();                 
        });
    }

    @Override
    public void stop(Future<Void> fut) throws Exception {
        if (server != null)
            server.close();
		serviceHosted.clear();
		serviceAddrs.clear();
		deviceRoute = null;
		getServiceRoute = null;
		setServiceRoute = null;
		waveformEventReportRoute = null;
		eventReportRoute = null;
		contextServiceRoute = null;
        SDCLib.getInstance().getLogger().log(Level.FINE, "Device HTTP server stopped.");
        fut.complete();             
    }
    
    
        
}
