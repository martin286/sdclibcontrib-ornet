/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.mdpws.impl.ws.routes;

import io.vertx.ext.web.Router;
import java.util.List;
import org.ornet.sdclib.binding.mdpws.impl.device.DeviceMsgManager;
import org.ornet.sdclib.binding.mdpws.impl.ws.WSConstants;

public class WaveformServicetRoute extends ServiceBaseRoute {
    
    public static final String REALM = WSConstants.SERVICE_ID_WAV;
    public static final String TYPE = WSConstants.PORT_TYPE_WAV;
    public static final String WSDL = "wavereport.wsdl";
    
    public void configure(Router router, DeviceMsgManager msgManager, List<String> serviceHosted, String wsdlPath, String wsdlFile) {
        configure(router, msgManager, REALM, TYPE, serviceHosted, wsdlPath, wsdlFile, null);
    }
    
}
