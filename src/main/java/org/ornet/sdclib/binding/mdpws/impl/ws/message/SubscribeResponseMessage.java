/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.mdpws.impl.ws.message;

import io.vertx.core.buffer.Buffer;
import javax.xml.soap.SOAPMessage;

public class SubscribeResponseMessage extends AbstractSoapMessage {

    private SubscribeResponseMessage() {
        super("/soap/SubscribeResponse.xml");
    }    
    
    public SubscribeResponseMessage(SOAPMessage content) {
        super(content);
    }
    
    public SubscribeResponseMessage(SubscribeMessage msg) {
        this();
        initResponse(msg);
    }
    
    public SubscribeResponseMessage(Buffer buffer) {
        super(buffer);
    }   

    private void initResponse(SubscribeMessage msg) {
        setRelatesTo(msg.getMessageId());
        setExpires(msg.getExpires());
    }
    
    public String getExpires() {
        return getTextNodeContent("Expires", true);
    }
    
    public void setExpires(String duration) {
        setTextNodeContent("Expires", duration, true);
    }

    public String getIdentifier() {
        return getTextNodeContent("Identifier", true);
    }
    
    public void setIdentifier(String ident) {
        setTextNodeContent("Identifier", ident, true);
    }
    
    public String getManagerAddr() {
        return getTextNodeContent("Address", true);
    }
    
    public void setManagerAddr(String addr) {
        setTextNodeContent("Address", addr, true);
    }    
    
}
