/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.mdpws.impl.client;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientResponse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerConfiguration;
import org.ornet.sdclib.binding.mdpws.impl.ws.WSConstants;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.AbstractSoapMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.GetMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.GetMetadataMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.GetMetadataResponseMessage;
import org.ornet.sdclib.binding.mdpws.impl.ws.message.GetResponseMessage;
import org.ornet.sdclib.common.SyncEvent;

public class MDPWSClient {
    
    public static final int TIMEOUT = 10000;
    
    private final LinkedList<String> xAddrs = new LinkedList<>();
    private final Vertx vertx;
    private final MDPWSTransportLayerConfiguration tlc;
    private final String epr;
    private String baseAddress;
    private int basePort;
    private boolean closed;
    private final HttpClient client;
    private final HttpClientOptions clientOptions;
    // Servide ID -> address
    private Map<String, String> hosted;
    private final Object hostedLock = new Object();

    public MDPWSClient(MDPWSTransportLayerConfiguration tlc, String xAddrs, String epr, Vertx vertx) {
        this.xAddrs.addAll(Arrays.asList(xAddrs.split(" ")));
        this.epr = epr;
        this.vertx = vertx;
        this.tlc = tlc;
        this.clientOptions = tlc.getConfigurationDetail().getClientOptions();
        this.client = vertx.createHttpClient(clientOptions);
    }

    public Vertx getVertx() {
        return vertx;
    }
    
    public void close() {
        if (!closed) {
            client.close();
            closed = true;
        }
    }
    
    public String getBaseAddress() {
        return baseAddress;
    }
    
    public int getBasePort() {
        return basePort;
    }
    
    public HttpClientOptions getOptions() {
        return clientOptions;
    }
    
    /*
    * Overriding default InetAddress.isReachable() method to add 2 more arguments port and timeout value
    */

    private boolean crunchifyAddressReachable(String address, int port, int timeout) {
	    try {
		    try (Socket crunchifySocket = new Socket()) {
			    // Connects this socket to the server with a specified timeout value.
			    crunchifySocket.connect(new InetSocketAddress(address, port), timeout);
		    }
		
		    // Return true if connection successful
		    return true;
	    } catch (IOException exception) {
			Logger.getLogger(MDPWSClient.class.getName()).log(Level.FINEST, "Socket not able to connect to: {0}", address + ":" + port);
		    return false;
	    }
    }
    
    public boolean reorderToReachable(List<String> addrs, boolean initBaseAddress) {
        String reachableAddr = null;
        for (String next : addrs) {
            try {
				URL url = new URL(next);
				if (crunchifyAddressReachable(url.getHost(), url.getPort(), getTlc().getConfigurationDetail().getCommTimeout())) {
				    SDCLib.getInstance().getLogger().log(Level.FINE, "Found reachable address: {0}", next);
				    reachableAddr = next;
				    final Socket socket = new Socket(url.getHost(), url.getPort());
				    if (initBaseAddress) {
				        String bindInterface = getTlc().getConfigurationDetail().getBindInterface();
				        baseAddress = socket.getLocalAddress().getHostAddress();
				        // Check if local address complies with bind interface
				        if (bindInterface.equals("0.0.0.0") || bindInterface.equals(baseAddress)) {				
				            socket.close();				
				            break;				
				        }				
				    } else {
				        socket.close();
				        break;
				    }
				}
            } catch (Exception ex) {
                Logger.getLogger(MDPWSClient.class.getName()).log(Level.FINE, "Not reachable: {0}", next);
            }
        }
        if (reachableAddr != null) {
            addrs.remove(reachableAddr);
            addrs.add(0, reachableAddr);
            return true;
        }
        return false;
    }

    public boolean buildUpBlocking(ClientMsgManager msgManager) {
        if (!reorderToReachable(xAddrs, true)) {
            SDCLib.getInstance().getLogger().log(Level.WARNING, "No reachable device found for {0}", epr);
            return false;
        }
        basePort = getTlc().getConfigurationDetail().extractNextPort();
        SDCLib.getInstance().getLogger().log(Level.FINE, "Remote endpoint build up for: {0}", xAddrs.get(0));
        // Get device & services info
        SyncEvent event = new SyncEvent();
        postDevice(new GetMessage(epr), (res) -> {
            res.bodyHandler((buf) -> {
                GetResponseMessage response = new GetResponseMessage(buf);
                if (!response.getAction().equals(WSConstants.GET_RESPONSE_ACTION)) {
                    SDCLib.getInstance().getLogger().log(Level.WARNING, "Suspicious response action received: {0}", response.getAction());
                }                
                SDCLib.getInstance().getLogger().log(Level.FINER, "Incoming message for client, Action: {0}", response.getAction());
                synchronized(hostedLock) {
                    hosted = new HashMap<>();
                    Map<String[], List<String>> serviceHosted = response.getServiceHosted();
                    serviceHosted.entrySet().forEach((next) -> {
                        if (!reorderToReachable(next.getValue(), false)) {
                            SDCLib.getInstance().getLogger().log(Level.WARNING, "No reachable service: {0}", next.getKey()[0]);
                        }
                        else {
                            final String type = next.getKey()[1];
                            final String eprAdr = next.getValue().get(0);
                            hosted.put(type, eprAdr); 
                            if (type.equals(WSConstants.PORT_TYPE_WAV)) {
                                getStreamingMetadata(type, eprAdr, msgManager);
                            }
                        }
                    });                    
                }
                event.set();
            });            
        }, (exc) -> {
            SDCLib.getInstance().getLogger().log(Level.WARNING, "Target unreachable: {0} ({1})", new Object[]{epr, exc.getMessage()});
            event.set(false);
        });
        if (!event.wait(TIMEOUT)) {
            SDCLib.getInstance().getLogger().log(Level.WARNING, "Metadata request failed for: {0}", epr);
            return false;
        }
        return true;
    }

    private void getStreamingMetadata(String type, String eprAdr, ClientMsgManager msgManager) {
        // Get metadata for streaming
        postService(type, new GetMetadataMessage(eprAdr), (resMeta) -> {
            resMeta.bodyHandler((bufMeta) -> {
                GetMetadataResponseMessage metaResponse = new GetMetadataResponseMessage(bufMeta);
                if (!metaResponse.getAction().equals(WSConstants.GET_META_RESPONSE_ACTION)) {
                    SDCLib.getInstance().getLogger().log(Level.WARNING, "Suspicious response action received: {0}", metaResponse.getAction());
                }
                SDCLib.getInstance().getLogger().log(Level.FINER, "Incoming message for client, Action: {0}", metaResponse.getAction());                        
                String streamAdr = metaResponse.getStreamAddress();
                if (streamAdr == null) {
                    SDCLib.getInstance().getLogger().log(Level.WARNING, "No stream address provided: {0}", eprAdr);
                    return;
                }
                SDCLib.getInstance().getLogger().log(Level.FINE, "Streaming endpoint found: {0}", streamAdr);
                msgManager.getStreamingManager().setStreamAdr(streamAdr);
                msgManager.getStreamingManager().initStreaming();
            });
        }, (exc) -> {
            SDCLib.getInstance().getLogger().log(Level.WARNING, "Target unreachable: {0} ({1})", new Object[]{eprAdr, exc.getMessage()});
        });
    }

    public HttpClient getClient() {
        return client;
    }
    
    public String getCurrentDeviceXAddr() {
        return xAddrs.getFirst();
    }
    
    public void postService(String servicePortType, AbstractSoapMessage msg, Handler<HttpClientResponse> hndlr, Handler<Throwable> excHandler) {
        String resolvedAddress;
        synchronized(hostedLock) {
            resolvedAddress = hosted.get(servicePortType);           
        }
        if (resolvedAddress == null) {
           SDCLib.getInstance().getLogger().log(Level.SEVERE, "Unknown remote port type: {0}", servicePortType);
           return;
        }
        msg.setTo(resolvedAddress);
        post(resolvedAddress, msg, hndlr, excHandler);        
    }
    
    public void postServiceRaw(String servicePortType, String string, Handler<HttpClientResponse> hndlr, Handler<Throwable> excHandler) {
        String resolvedAddress;
        synchronized(hostedLock) {
            resolvedAddress = hosted.get(servicePortType);           
        }
        postRaw(resolvedAddress, Buffer.buffer(string), hndlr, excHandler);        
    }    

    public void postDevice(AbstractSoapMessage msg, Handler<HttpClientResponse> hndlr, Handler<Throwable> excHandler) {
        String devAdr = getCurrentDeviceXAddr();
        post(devAdr, msg, hndlr, excHandler);        
    }

    private void post(String absAddr, AbstractSoapMessage msg, Handler<HttpClientResponse> hndlr, Handler<Throwable> excHandler) {
        final Buffer buffer = msg.createBuffer();
        postRaw(absAddr, buffer, hndlr, excHandler);
    }

    private void postRaw(String absAddr, Buffer buffer, Handler<HttpClientResponse> hndlr, Handler<Throwable> excHandler) {
        SDCLib.getInstance().getLogger().log(Level.FINEST, "HTTP client side post to address: {0}", absAddr);
        client.postAbs(absAddr, (res) -> {
            hndlr.handle(res);
        }).setTimeout(tlc.getConfigurationDetail().getCommTimeout())
				.exceptionHandler(excHandler)
                .putHeader("Content-Type", "application/soap+xml")
                .setChunked(true)
                .end(buffer);        
    }

    public MDPWSTransportLayerConfiguration getTlc() {
        return tlc;
    }
    
}
