/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding.device;

import java.math.BigInteger;
import org.ornet.cdm.AbstractAlertReport;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextReport;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricReport;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.EpisodicAlertReport;
import org.ornet.cdm.EpisodicContextReport;
import org.ornet.cdm.EpisodicMetricReport;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationError;
import org.ornet.cdm.InvocationInfo;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.OperationInvokedReport;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.WaveformStream;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.OperationInvocationContext;
import org.ornet.sdclib.provider.SDCProvider;

public abstract class BICEPSProviderEventSourceBinding implements IProviderEventSourceBinding {
    
    private final SDCProvider provider;
    
    public BICEPSProviderEventSourceBinding(SDCProvider provider) {
        this.provider = provider;
    }
    
    @Override
   public void handleEpisodicMetricEvent(AbstractMetricState state, BigInteger mdibVersion) {
        AbstractMetricReport.ReportPart mrp = new AbstractMetricReport.ReportPart();
        mrp.getMetricState().add(state);
        EpisodicMetricReport report = new EpisodicMetricReport();
        report.getReportPart().add(mrp);
        report.setMdibVersion(mdibVersion);
        report.setSequenceId("0");
        fireEpisodicMetricEventReport(report);        
    }
    
    @Override
    public void handleEpisodicAlertEvent(AbstractAlertState state, BigInteger mdibVersion) {
        AbstractAlertReport.ReportPart arp = new AbstractAlertReport.ReportPart();
        arp.getAlertState().add(state);
        EpisodicAlertReport report = new EpisodicAlertReport();
        report.getReportPart().add(arp);
        report.setMdibVersion(mdibVersion);
        report.setSequenceId("0");
        fireEpisodicAlertEventReport(report);  
    }    

    @Override
    public void handleOperationInvokedEvent(OperationInvocationContext oic, InvocationState is, BigInteger mdibVersion, String operationErrorMsg) {
        OperationInvokedReport.ReportPart orp = new OperationInvokedReport.ReportPart();
        InvocationInfo invInf = new InvocationInfo();
        orp.setInvocationInfo(invInf);
        orp.setInvocationSource(new InstanceIdentifier());
        invInf.setTransactionId(oic.getTransactionId());
        invInf.setInvocationState(is);
        orp.setOperationHandleRef(oic.getOperationHandle());
        orp.setOperationTarget(SDCToolbox.getOperationTargetForOperationHandle(provider, oic.getOperationHandle()));
        if (operationErrorMsg != null) {
            invInf.setInvocationError(InvocationError.UNKN);
        }
        OperationInvokedReport oir = new OperationInvokedReport();
        oir.getReportPart().add(orp);
        oir.setMdibVersion(mdibVersion);
        oir.setSequenceId("0");
        fireOperationInvokedEventReport(oir);          
    }

    @Override
    public void handleEpisodicContextChangedEvent(AbstractContextState acs, BigInteger mdibVersion) {
        AbstractContextReport.ReportPart crp = new AbstractContextReport.ReportPart();
        crp.getContextState().add(acs);
        EpisodicContextReport ecr = new EpisodicContextReport();
        ecr.getReportPart().add(crp);
        ecr.setMdibVersion(mdibVersion);
        ecr.setSequenceId("0");
        fireEpisodicContextEventReport(ecr);  
    }    
    
    @Override
    public void handleStream(RealTimeSampleArrayMetricState state) {
        WaveformStream wfs = new WaveformStream();
        wfs.getState().add(state);
        wfs.setSequenceId("0");
        sendStream(wfs);
    }    
    
}
