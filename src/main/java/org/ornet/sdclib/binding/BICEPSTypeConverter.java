/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Pulic License version 3.0.
 * http://www.gnu.org/licenses/gpl-3.0.de.html
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.sdclib.binding;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class BICEPSTypeConverter {
    
    public static <T> T toType(String xml, Class<T> target) {
        try {
            Unmarshaller m = JAXBUtil.getInstance().getUnmarshaller(target);
            return target.cast(m.unmarshal(new ByteArrayInputStream(xml.getBytes(Charset.defaultCharset()))));
        } catch (JAXBException ex) {
            Logger.getLogger(BICEPSTypeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static <T> String toString(T source) {
        try {
            Marshaller m = JAXBUtil.getInstance().getMarshaller(source.getClass());
            StringWriter writer = new StringWriter();
            m.marshal(source, writer);
            return writer.toString();
        } catch (JAXBException ex) {
            Logger.getLogger(BICEPSTypeConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
